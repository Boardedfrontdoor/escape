This repository contains the sources for the XVAN textadventure "Escape".

escape.xvn is the game source

ENG-starterkit is the XVAN starterkit that is included by escape.xvn

my-includes contains addtions to and/or redefinitions of items in the starterkit specific for this game.

Images contains images that are used with the IFI-versin of the interpreter (yet to be released).

Testinput.txt is a command input file used for regression testing.
